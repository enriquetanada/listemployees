@extends ('layouts.app')

@section('content')
	<div class="container container-fluid">
		{{-- header start --}}
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					List of Employees
				</h1>
			</div>
		</div>
		{{-- header end --}}
		{{-- alert message --}}
		@includeWhen(Session::has('message'),'partials.alert')
		<div class="row">
			@foreach($employees as $employee)
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 mx-auto">
					{{-- employee card start --}}
					<div class="card">
						<img src="{{'http://localhost:8000/images/units/' . $employee->image }}" alt="" class="card-img-top">
						<div class="card-body">
							<h4 class="text-center">{{$employee->lastname}}, {{$employee->firstname}}</h4>
							<p>Email: {{$employee->email}}</p>
							<p>Password: {{$employee->password}}</p>
							<p>Username: {{$employee->username}}</p>
							<p>Contact Number: {{$employee->contactno}}</p>
						</div>
					</div>
					<div class="card-footer text-center">
						@include('employees.partials.edit')
						@include('employees.partials.delete')
					</div>
					{{-- employee card end --}}
				</div>
			@endforeach
		</div>
	</div>
@endsection