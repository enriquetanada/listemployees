@extends ('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Edit Employee
				</h1>
			</div>
		</div>
		{{-- start of form section --}}
		<div class="row">
			<div class="col-12 col-sm-6 mx-auto">
				<form 
					action="{{route('employees.update', $employee->id)}}" 
					method="post"
					enctype="multipart/form-data"
				>
					@csrf
					@method('PUT')
					{{-- first_name --}}
					@include('employees.partials.form-group',[
						'name'=> 'firstname',
						'type'=> 'text',
						'value' => $employee->firstname,
						'classes'=> ['form-control', 'form-control-sm']
					])
					{{-- last_name --}}
					@include('employees.partials.form-group',[
						'name'=> 'lastname',
						'type'=> 'text',
						'value' => $employee->lastname,
						'classes'=> ['form-control', 'form-control-sm']
					])
					{{-- username --}}
					@include('employees.partials.form-group',[
						'name'=> 'username',
						'type'=> 'text',
						'value' => $employee->username,
						'classes'=> ['form-control', 'form-control-sm']
					])
					{{-- email --}}
					@include('employees.partials.form-group',[
						'name'=> 'email',
						'type'=> 'email',
						'value' => $employee->email,
						'classes'=> ['form-control', 'form-control-sm']
					])
					{{-- password --}}
					@include('employees.partials.form-group',[
						'name'=> 'password',
						'type'=> 'password',
						'value' => $employee->password,
						'classes'=> ['form-control', 'form-control-sm']
					])
					{{-- contactno --}}
					@include('employees.partials.form-group',[
						'name'=> 'contactno',
						'type'=> 'text',
						'value' => $employee->contactno,
						'classes'=> ['form-control', 'form-control-sm']
					])
					{{-- image --}}
					@include('employees.partials.form-group',[
						'name'=> 'image',
						'type'=> 'file',
						'classes'=> ['form-control-file', 'form-control-sm']
					])

					<button class="btn btn-sm btn-primary mt-3 w-100">
						Edit Employee
					</button>


				</form>
			</div>
		</div>
		{{-- end of form section --}}
	</div>

@endsection