<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all()->sortBy('last_name');
        return view('employees.index')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'contactno'=> 'required|numeric',
            'username' => 'required|string',
            'image' =>    'required|image|max:2000',
            'email' => 'required|unique:employees,email',
            'password' => 'required|string' 
        ]);


        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('/images/units'),$imageName);
        $employee = new Employee($validatedData);
        $employee->image = $imageName;
        $employee->password = Hash::make($request->password);
        $employee->save();
        return redirect(route('employees.index'))
            ->with('message', "Employee {$employee->lastname},{$employee->firstname} is added successfully") ;   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit')
            ->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $validatedData = $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'contactno'=> 'required|numeric',
            'username' => 'required|string',
            'image' =>    'image|max:2000',
            'email' => 'required|unique:employees,email',
            'password' => 'required|string' 
        ]);
        $employee->update($validatedData);

         if($request->hasFile('image')){
           $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/images/units'),$imageName);
            $employee->image = $imageName;
        }
        $employee->password = Hash::make($request->password);
        $employee->save();
        return redirect(route('employees.index'))
            ->with('message',"Employee is updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
       $employee->delete();
       return redirect(route('employees.index'))->with('message','Employee is deleted successfully')->with('alert',"danger");

    }
}
