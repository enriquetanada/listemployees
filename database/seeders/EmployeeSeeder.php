<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;



class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
        	'firstname'=> 'Steve',
        	'lastname' => 'Rogers',
        	'email' => 'steve@email.com',
        	'contactno' => '09172619471',
        	'image' => 'https://via.placeholder.com/150',
        	'username' => 'steverogers',
        	'password' =>  Hash::make('test1234')
        ]);
        DB::table('employees')->insert([
        	'firstname'=> 'Clark',
        	'lastname' => 'Kent',
        	'email' => 'clark@email.com',
        	'contactno' => '09235412562',
        	'image' => 'https://via.placeholder.com/150',
        	'username' => 'clarkkent',
        	'password' =>  Hash::make('test1234')
        ]);
    }
}
